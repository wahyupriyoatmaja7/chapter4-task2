// 1. bikin class Vehicle (Kendaraan)
class Vehicle {
    // attribut nya (constructor) : jenis kendaraan (roda berapa 2/4 atau lainnya), negara produksi, 
    // ada method info yang melakukan print = 'Jenis Kendaraan roda 4 dari negara Jerman'
    constructor(jenis_kendaraan, negara_produksi){
        this.jenis_kendaraan = jenis_kendaraan;
        this.negara_produksi = negara_produksi;
    }
    info() {
        console.log(`jenis kendaraan roda ${this.jenis_kendaraan} Berasal dari negara ${this.negara_produksi}`);
    }

}
// 2. bikin child class (Mobil) dari Kendaraan, inherit attribut jenis kendaraan dan negara produksi dari super/parent class nya
class Mobil extends Vehicle {
    // attribut baru di child class ini yaitu Merek Kendaraan, Harga Kendaraan dan Persen Pajak
    constructor(jenis_kendaraan, negara_produksi, merk_kendaraan, harga_kendaraan, persentasi_pajak, potongan_harga) {
        super(jenis_kendaraan, negara_produksi)
        this.merk_kendaraan=merk_kendaraan;
        this.harga_kendaraan=harga_kendaraan;
        this.persentasi_pajak=persentasi_pajak;
        this.potongan_harga=potongan_harga;
    }
    // 3. ada method totalPrice yang melakukan proses menambah harga normal dengan persen pajak, yang RETURN hasil penjumlahan tersebut
    total_price() {
        // total price dari harga kendaraan plus total pajak dan diskon
        return (this.harga_kendaraan + (this.harga_kendaraan * this.persentasi_pajak / 100)) - this.harga_kendaraan * this.potongan_harga / 100;
    }
    info() {
        super.info();
        
        // print = 'Kendaraan ini nama mereknya Mercedes dengan total harga Rp. 880.000.000'
        console.log(`kendaraan ini bermerk ${this.merk_kendaraan} dengan harga Rp. ${this.total_price()}`);
    }
}

//buat 2 instance (1 dari parent class(Vehicle), 1 dari child class aja(Mobil)) 
const kendaraaan = new Vehicle(2,'japan')
kendaraaan.info()
const mobil = new Mobil(4, 'indonesia', 'suzuki', 450000000, 10, 3);
mobil.info()